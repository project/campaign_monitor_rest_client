<?php

namespace Drupal\campaign_monitor_rest_client\Http;

use CampaignMonitor\CampaignMonitorRestClient;

class CampaignMonitorRestClientDisabled extends CampaignMonitorRestClient {

  /**
   *
   */
  public function requestAsync($method, $uri = '', array $options = []) {
    throw new \Exception('Campaign Monitor REST Client is disabled.');
  }

}
