# Campaign Monitor REST API Client Service

This module provides a `campaign_monitor_rest_client` service that configures and wraps the super-simple [Campaign Monitor REST API Client][]. It is intended for use by developers who need a very basic REST API client for the [Campaign Monitor API][].

It works just like the `http_client` service from core. In fact, it returns a client that is fully compatible with `http_client`, but tailored specifically for use with the Campaign Monitor API.

This module differs from other Campaign Monitor modules in that it only provides a service that facilitates API access to Campaign Monitor using a much simpler library than the official `createsend-php` package.

## Usage

Enable the `campaign_monitor_rest_client` module and configure it at `/admin/config/services/campaign_monitor_rest_client`. At the moment, the only available authentication method is via an [API key][].

Once enabled and configured, you can use the `campaign_monitor_rest_client` service as you would any other service. E.g.:

```php
$client = \Drupal::service('campaign_monitor_rest_client');
```

Or via injection in a class that implements `ContainerInjectionInterface`:

```php
/**
 * {@inheritdoc}
 */
public static function create(ContainerInterface $container) {
  return new static(
    $container->get('campaign_monitor_rest_client')
  );
}
```

Or injection into a service class:

```yml
services:
  custom_campaigns.course_notifications:
    class: Drupal\custom_campaigns\CourseNotificationHelper
    arguments: ['@campaign_monitor_rest_client', '@entity_type.manager', '@config.factory']
```

Once you've assigned the service to a variable, usage is very similar to Guzzle:

```php
$client_response = $client->get('clients.json');
```

See the [Campaign Monitor REST API Client][] readme for more information on how to use the client.

[Campaign Monitor REST API Client]: https://github.com/ilrWebServices/campaign-monitor-rest-api-client
[Campaign Monitor API]: https://www.campaignmonitor.com/api
[API key]: https://www.campaignmonitor.com/api/getting-started/#authenticating-api-key
